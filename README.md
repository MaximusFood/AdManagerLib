# Max's AdManager Lib
## v1.4.1

A static library for helping to display interstitial ads from various frameworks in an iPhone app. Written in Objective-C.

I was getting a bit fed up with copying my AdManager class around from project to project, so I thought I'd chuck it into a lovely static lib, formalise the process a bit, and simplify the dependency management with CocoaPods.

## Supported Ad Frameworks
- AdMob
- Chartboost
- iAd
- Flurry 
- Vungle
- RevMob

## SETUP
- Add the library to the 'Link Binary With Libraries' bit of your project.
- Add the library headers to your project.
- Now, we also have to add the dependencies for all the ads. Luckily, there is nothing to it. We just copy the Podfile into the same dir as your project's xproj file, change the 'target' in the podfile to the target in your project, and run <code>pod install</code> in the terminal. There may be a warning or two to clear up. From now on use the workspace file, not the project file.
- Might have to set 'build active architecture only' to 'no' in you pods project.


## UPDATE
- Copy the Podfile, the libAdManager.a, and the headers.
- Run <code>pod install</code> if a new framework, or new framework version.
- In case of pod install, may have to set 'build active architecture only' to 'no' in you pods project again.


## USAGE
See the AdManager.h for public properties and methods and descriptions. There is a separate 'controller' for each framework, which can be accessed for any framework specific stuff. Also there's an AdManagerObserver protocol, for some useful callbacks. Definition is in AdManager.h as well. 

Example set-up in Swift:

```swift
class GameViewController: UIViewController {
    override func viewDidLoad() {
        
        ...
    
        // Set up AdManager
        AdManager.getManager().uiViewController = self
        
        AdManager.getManager().chartboostController.cbAppID = CHARTBOOST_APP_ID
        AdManager.getManager().chartboostController.cbAppSig = CHARTBOOST_APP_SIG
        
        AdManager.getManager().adMobController.adMobUnitID = ADMOB_UNIT_ID
        AdManager.getManager().adMobController.adMobTestDevices = [DEVICE1_ID, DEVICE2_ID]
        
        AdManager.getManager().chanceOfAttempt = 0.4
        AdManager.getManager().cooldownAttempts = 3
        
        AdManager.getManager().priorityList = [kFrameworkTypeChartboost : 2,
                                                    kFrameworkTypeAdMob : 2,
                                                      kFrameworkTypeIAd : 1]
        
        AdManager.getManager().prepareAdFrameworks()

        ...
    }

...
}
```

And to attempt to show an ad, if outside the cooldown period, and subject to the <code>chanceOfAttempt</code> property:

```swift
...

AdManager.sharedInstance().attemptAdIfReady()

...
```

