//
//  AdManager.m
//
//  Created by Maxim Arnold on 14/10/2015.
//  Copyright © 2015 Maxim. All rights reserved.
//

#import "AdManager.h"
#import "AdManager+Internal.h"

#import "AdFrameworkController+Internal.h"
#import "AdFrameworkControllers.h"


@implementation AdManager
{
    /*
     *  Shared Vars
     */
    
    UIViewController* m_uiViewController; // must be set
    int m_adAttemptsSinceLastAd;
    
    BOOL m_suppressAllAds;
    
    NSDictionary* m_priorityList;
    NSMutableArray* m_remainingFrameworks;
    
    int m_cooldownAttempts;
    double m_chanceOfAttempt;
    
    // If have failed to show any ad at all, we want to keep trying utill we do.
    BOOL m_ignoreChanceOfAttempt;
    
    BOOL m_adHasBeenShown;
    
    NSMutableArray* m_adManagerObservers;
    

    /*
     *  AdFrameworkControllers
     */
    ChartboostController* m_chartboostController;
    AdMobController* m_adMobController;
    IAdController* m_iAdController;
    FlurryController* m_flurryController;
    VungleController* m_vungleController;
    RevMobController* m_revMobController;
    
    NSArray* m_frameworkControllers;
}

/*
 *  Property Synthesis
 */

@synthesize uiViewController = m_uiViewController;
@synthesize priorityList = m_priorityList;

@synthesize suppressAllAds = m_suppressAllAds;
@synthesize adHasBeenShown = m_adHasBeenShown;

@synthesize cooldownAttempts = m_cooldownAttempts;
@synthesize chanceOfAttempt = m_chanceOfAttempt;

// Override setVal. Make sure it's within range.
- (void)setChanceOfAttempt:(double)chance
{
    if (chance < 0)
    {
        m_chanceOfAttempt = 0;
    }
    else if(chance > 1)
    {
        m_chanceOfAttempt = 1;
    }
    else
    {
        m_chanceOfAttempt = chance;
    }
}

@synthesize chartboostController = m_chartboostController;
@synthesize iAdController = m_iAdController;
@synthesize adMobController = m_adMobController;
@synthesize flurryController = m_flurryController;
@synthesize vungleController = m_vungleController;
@synthesize revMobController = m_revMobController;


#pragma mark - Class Methods -

+ (AdManager *)sharedInstance
{
    static AdManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedInstance = [[AdManager alloc] init];
    });
    
    return sharedInstance;
}


#pragma mark -

/*
 *  Initialisation
 */

- (id) init
{
    self = [super init];
    if(!self) return nil;
    
    // Defaults to all having an equal priority, so chosen at random.
    m_priorityList = @{
                       kFrameworkTypeIAd        : @1,
                       kFrameworkTypeAdMob      : @1,
                       kFrameworkTypeChartboost : @1,
                       kFrameworkTypeFlurry     : @1,
                       kFrameworkTypeVungle     : @1,
                       kFrameworkTypeRevMob     : @1
                       };
    
    m_adAttemptsSinceLastAd = 0;
    m_cooldownAttempts = 5; // default to 5
    
    m_chanceOfAttempt = 0.5; // default to one in two chance
    
    m_ignoreChanceOfAttempt = NO;
    
    m_suppressAllAds = NO;
    
    m_adHasBeenShown = NO;
    
    m_adManagerObservers = [NSMutableArray array];
    
    // Fire up all the framework controllers
    m_chartboostController = [ChartboostController controller];
    m_adMobController = [AdMobController controller];
    m_iAdController = [IAdController controller];
    m_flurryController = [FlurryController controller];
    m_vungleController = [VungleController controller];
    m_revMobController = [RevMobController controller];
    
    m_frameworkControllers = @[m_chartboostController,
                               m_adMobController,
                               m_iAdController,
                               m_flurryController,
                               m_vungleController,
                               m_revMobController];
    
    return self;
}


#pragma mark - Public Methods -

/*
 *  Public Methods
 */

- (void)prepareAdFrameworks
{
    if( ![self checkValidSetup] )
        return;
    
    for(AdFrameworkController* controller in m_frameworkControllers)
    {
        [controller prepareFramework];
    }
}


- (void)attemptAdIfReady
{
    if( ![self checkValidSetup] )
        return;
    
    // So we take whatever the chance is, multiply it by 100, and then generate a number
    // from 0 - 99 inclusive. If the rand is less than our 'chance' figure, we're in
    // business.
    int chance = m_chanceOfAttempt*100;
    int rand = arc4random_uniform(100);

    if( ++m_adAttemptsSinceLastAd > m_cooldownAttempts
       && (rand < chance || m_ignoreChanceOfAttempt) ) // have to do this check second, as we're incing
    {
        // Prepare a mutable list of remaining frameworks to try
        m_remainingFrameworks = [[m_priorityList allKeys] mutableCopy];
        
        // And then kick off the first attempt.
        [self attemptShowAd];
    }
}


- (void)addAdManagerObserver:(NSObject<AdManagerObserver>*)observer
{
    [m_adManagerObservers addObject:observer];
}


- (void)removeAdManagerObserver:(NSObject<AdManagerObserver>*)observer
{
    [m_adManagerObservers removeObject:observer];
}


#pragma mark - Private Methods -

/*
 *  Private Helpers
 */

/*
 *  Here we try and show an ad, starting with the first dude in the queue, and keep on
 *  down the queue until we have success.
 */
- (void)attemptShowAd
{
    if( ![self checkValidSetup] )
        return;
    
    NSString* nextFrameworkAttempt = [self getNextFramework];
    
    BOOL success = [[self controllerForFrameworkType:nextFrameworkAttempt] attemptShowAd];
    
    // If success, reset the attempts. Of course, this could actually be a failure, and
    // we may find out in one of the framework delegate methods, so we can't do any list
    // resetting here.
    if( success )
    {
        m_adAttemptsSinceLastAd = 0;
        m_ignoreChanceOfAttempt = NO;
        m_adHasBeenShown = YES; // Might not be true, so remember to set to NO in failure
                                // methods
    }
    // If unsuccessful and we have more frameworks to try, fire off attemptShowAd once
    // more.
    else if( [m_remainingFrameworks count] > 0 )
    {
        [self attemptShowAd];
    }
    // If it's the end of our framework list, then I'm afraid that's that for now. We
    // won't reset m_adAttemptsSinceLastAd, and we'll set a flag to ignore the chance
    // factor, so that the next call to attemptAdIfReady will result in an attempt.
    else if( [m_remainingFrameworks count] == 0 )
    {
        m_ignoreChanceOfAttempt = YES;
        NSLog(@"End of the queue I'm afraid; no ads successfully shown.");
    }
}

/*
 *  Contains all the clever logic that manages the priority list.
 */
- (NSString *)getNextFramework
{
    if( [m_remainingFrameworks count] < 1 )
        return @"";
    
    NSString* nextFramework = @"";
    
    // We could have more than one framework with the same priority, so we collect all the
    // frameworks with the lowest priority and randomly choose between them.
    NSMutableArray* m_frameworksToTry = [NSMutableArray new];
    
    int lowestPriority = 99999;
    
    for(id fw in m_remainingFrameworks)
    {
        NSNumber* value = (NSNumber *)m_priorityList[fw];
        if( [value intValue] == lowestPriority )
        {
            // So if we've found one of equal priority, chuck it in.
            [m_frameworksToTry addObject:fw];
        }
        else if( [value intValue] < lowestPriority )
        {
            // But if we find a new lowest one, then ditch the old list and start a new
            // one.
            lowestPriority = [value intValue];
            m_frameworksToTry = [NSMutableArray new];
            [m_frameworksToTry addObject:fw];
        }
    }
    
    // Grab a random position in the array
    int pos = arc4random_uniform((int)[m_frameworksToTry count]);
    nextFramework = m_frameworksToTry[pos];
    
    // And remove that framework from the remaining Priorities.
    [m_remainingFrameworks removeObject:nextFramework];
    
    return nextFramework;
}

/*
 *  This check should be used at the start of all public methods. Return FALSE if we do
 *  not have all the bits we need.
 */
- (BOOL)checkValidSetup
{
    if( m_uiViewController == nil )
    {
        NSLog(@"Invalid AdManager setup.");
        return FALSE;
    }
    
    if( m_suppressAllAds )
    {
        NSLog(@"suppressAllAds is set to true.");
        return FALSE;
    }
    
    return TRUE;
}

/*
 *  Returns AdFrameworkController based on passed framework type;
 */
- (AdFrameworkController *)controllerForFrameworkType:(NSString *)frameworkType
{
    AdFrameworkController* retVal = nil;
    
    for(AdFrameworkController* controller in m_frameworkControllers)
    {
        if( [controller.frameworkType isEqualToString:frameworkType] )
        {
            retVal = controller;
            break;
        }
    }
    
    return retVal;
}


@end


#pragma mark - Internal Methods -
/*
 *  Internal Implementation
 */

@implementation AdManager (Internal)


- (void)failedToShowAd:(BOOL)firstAd
{
    // We will have already called 'success', so we can put back the cooldown period.
    m_adAttemptsSinceLastAd = m_cooldownAttempts;
    
    // If this was the ad that flipped the adHasBeenShown switch, then we need to switch
    // it back.
    if( firstAd )
        m_adHasBeenShown = NO;
    
    // The remainingFrameworks list will still be in good shape, so just fire off another
    // attempt.
    if( [self checkValidSetup] )
        [self attemptShowAd];
}


- (void)adWasDismissed
{
    for(NSObject<AdManagerObserver>* observer in m_adManagerObservers)
    {
        if( [observer respondsToSelector:@selector(adDismissed)])
            [observer adDismissed];
    }
}


@end
