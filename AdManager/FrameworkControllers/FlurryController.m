//
//  FlurryController.m
//  AdManager
//
//  Created by Maxim Arnold on 22/11/2015.
//  Copyright © 2015 Maxim. All rights reserved.
//

#import "FlurryController.h"
#import "AdFrameworkController+Internal.h"
#import "AdManager+Internal.h"

@implementation FlurryController
{
    NSString* m_apiKey;
    NSString* m_adSpaceName;
    
    BOOL m_testMode;
    
    FlurryAdInterstitial* m_adInterstitial;
}

@synthesize apiKey = m_apiKey;
@synthesize adSpaceName = m_adSpaceName;
@synthesize testMode = m_testMode;

/*
 *  Initialisation
 */

// Factory initialiser.
+ (id)controller
{
    return [[self alloc] initWithFrameworkType:kFrameworkTypeFlurry];
}

- (id)initWithFrameworkType:(NSString *)frameworkType
{
    self = [super initWithFrameworkType:frameworkType];
    if(!self) return nil;
    
    m_testMode = FALSE;
    
    return self;
}

#pragma mark - Private Methods -
/*
 *  Private helpers.
 */

- (void)loadNewAd
{
    m_adInterstitial = [[FlurryAdInterstitial alloc] initWithSpace:m_adSpaceName];
    m_adInterstitial.adDelegate = self;
    
    if( m_testMode )
    {
        FlurryAdTargeting* adTargeting = [FlurryAdTargeting targeting];
        adTargeting.testAdsEnabled = YES;
        m_adInterstitial.targeting = adTargeting;
    }
    
    [m_adInterstitial fetchAd];
}


#pragma mark - Internal Methods -
/*
 *  Overridden Internal Methods
 */

- (void)prepareFramework
{
    if( ![m_apiKey isEqualToString:@""] && ![m_adSpaceName isEqualToString:@""] )
    {
        // With this done successfully you can use Flurry analytics anywhere in your app.
        [Flurry startSession:m_apiKey];
        
        [self loadNewAd];
    }
}

- (BOOL)attemptShowAd
{
    BOOL success = FALSE;
    
    NSLog(@"Attempting to show an Flurry ad...");
    
    if ([m_adInterstitial ready])
    {
        [m_adInterstitial presentWithViewController:[AdManager sharedInstance].uiViewController];
        success = TRUE;
        NSLog(@"Should be seeing a Flurry ad");
    }
    else
    {
        [m_adInterstitial fetchAd];
    }
    
    return success;
}


#pragma mark - Delegate Methods -
/*
 *  Methods from the FlurryAdInterstitialDelegate protocol
 */

- (void)adInterstitial:(FlurryAdInterstitial*)interstitialAd
                adError:(FlurryAdError) adError
       errorDescription:(NSError*) errorDescription
{
    NSLog(@"Flurry interstitialAd didFailWithERROR");
    NSLog(@"%@", errorDescription);
}

//Informs the app that a video associated with this ad has finished playing.
//Only present for rewarded & client-side rewarded ad spaces
- (void)adInterstitialVideoDidFinish:(FlurryAdInterstitial *)interstitialAd
{
    NSLog(@"Flurry video did finish");
}

- (void)adInterstitialDidDismiss:(FlurryAdInterstitial *)interstitialAd
{
    NSLog(@"Flurry interstitial did dismiss");
    
    [[AdManager sharedInstance] adWasDismissed];
}


@end
