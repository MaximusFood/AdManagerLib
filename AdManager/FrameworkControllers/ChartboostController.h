//
//  ChartboostController.h
//  AdManager
//
//  Created by Maxim Arnold on 21/11/2015.
//  Copyright © 2015 Maxim. All rights reserved.
//

#import "AdFrameworkController.h"
#import <Chartboost/Chartboost.h>


@interface ChartboostController : AdFrameworkController <ChartboostDelegate>

// Essential for cb to work
@property(nonatomic, strong) NSString* cbAppID;
@property(nonatomic, strong) NSString* cbAppSig;


+ (id)controller;

@end
