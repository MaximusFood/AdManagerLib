//
//  AdFrameworkController+Internal.h
//  AdManager
//
//  Created by Maxim Arnold on 21/11/2015.
//  Copyright © 2015 Maxim. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AdFrameworkController.h"


@interface AdFrameworkController (Internal)

// Needs the framework type. The derived dudes are gonna be using this initialiser.
- (id)initWithFrameworkType:(NSString *)frameworkType;

// Not all frameworks need to do things, but many do.
- (void)prepareFramework;

// And here we have the attempt itself. This is just for the AdManager really.
- (BOOL)attemptShowAd;

@end
