//
//  FlurryController.m
//  AdManager
//
//  Created by Maxim Arnold on 22/11/2015.
//  Copyright © 2015 Maxim. All rights reserved.
//

#import "VungleController.h"
#import "AdFrameworkController+Internal.h"
#import "AdManager+Internal.h"

@implementation VungleController
{
    NSString* m_appID;
}

@synthesize appID = m_appID;


/*
 *  Initialisation
 */

// Factory initialiser.
+ (id)controller
{
    return [[self alloc] initWithFrameworkType:kFrameworkTypeVungle];
}


#pragma mark - Internal Methods -
/*
 *  Overridden Internal Methods
 */

- (void)prepareFramework
{
    if(![m_appID isEqualToString:@""])
    {
        VungleSDK* sdk = [VungleSDK sharedSDK];
        [sdk startWithAppId:m_appID];
        
        [[VungleSDK sharedSDK] setDelegate:self];
        [[VungleSDK sharedSDK] setLoggingEnabled:YES];
    }
}

- (BOOL)attemptShowAd
{
    BOOL success = FALSE;
    
    NSLog(@"Attempting to show a Vungle ad...");
    
    VungleSDK* sdk = [VungleSDK sharedSDK];
    
    if([sdk isAdPlayable])
    {
        NSError *error;
        if([sdk playAd:[AdManager sharedInstance].uiViewController error:&error])
        {
            success = TRUE;
            NSLog(@"Should be seeing a Vungle ad");
        }
        else
        {
            NSLog(@"Vungle interstitialAd didFailWithERROR");
            NSLog(@"%@", error);
        }
    }
    else
    {
        NSLog(@"Vungle isn't ready!");
    }
    
    return success;
}


#pragma mark - Delegate Methods -
/*
 *  Methods from the VungleSDKDelegate protocol
 */

- (void)vungleSDKwillCloseAdWithViewInfo:(NSDictionary*)viewInfo
                 willPresentProductSheet:(BOOL)willPresentProductSheet
{
    if(!willPresentProductSheet)
    {
        NSLog(@"Vungle interstitial did dismiss");
        [[AdManager sharedInstance] adWasDismissed];
    }
}


@end
