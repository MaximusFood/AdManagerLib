//
//  AdMobController.h
//  AdManager
//
//  Created by Maxim Arnold on 21/11/2015.
//  Copyright © 2015 Maxim. All rights reserved.
//

#import "AdFrameworkController.h"
@import GoogleMobileAds;


@interface AdMobController : AdFrameworkController <GADInterstitialDelegate>

// Essential for AdMob to work.
@property(nonatomic, strong) NSString* adMobUnitID;

// AdMob allows a bunch of test device ids to be provided, meaning you'll only be served
// test ads on those devices. Useful for avoiding an account suspension.
@property(nonatomic, strong) NSArray* adMobTestDevices;


+ (id)controller;

@end
