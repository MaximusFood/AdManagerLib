//
//  IAdController.h
//  AdManager
//
//  Created by Maxim Arnold on 21/11/2015.
//  Copyright © 2015 Maxim. All rights reserved.
//

#import "AdFrameworkController.h"
#import <iAd/iAd.h>


@interface IAdController : AdFrameworkController <ADInterstitialAdDelegate>

+ (id)controller;

@end
