//
//  AdMobController.m
//  AdManager
//
//  Created by Maxim Arnold on 21/11/2015.
//  Copyright © 2015 Maxim. All rights reserved.
//

#import "AdMobController.h"
#import "AdFrameworkController+Internal.h"
#import "AdManager+Internal.h"


@implementation AdMobController
{
    GADInterstitial* m_interstitial;
    NSString* m_adMobUnitID;
    NSArray* m_adMobTestDevices;
}


@synthesize adMobUnitID = m_adMobUnitID;
@synthesize adMobTestDevices = m_adMobTestDevices;


/*
 *  Initialisation
 */

// Factory initialiser.
+ (id)controller
{
    return [[self alloc] initWithFrameworkType:kFrameworkTypeAdMob];
}

- (id)initWithFrameworkType:(NSString *)frameworkType
{
    self = [super initWithFrameworkType:frameworkType];
    if(!self) return nil;
    
    m_adMobUnitID = @"ca-app-pub-3940256099942544/4411468910"; // Default to test ID
    
    return self;
}


#pragma mark - Internal Methods -
/*
 *  Overridden Internal Methods
 */

- (void)prepareFramework
{
    [self preloadAdMobAd];
}

- (BOOL)attemptShowAd
{
    BOOL success = FALSE;
    
    NSLog(@"Attempting to show an AdMob ad...");
    
    if( [m_interstitial isReady] )
    {
        [m_interstitial presentFromRootViewController:[AdManager sharedInstance].uiViewController];
        success = TRUE;
        NSLog(@"Should be seeing an AdMob ad");
    }
    
    return success;
}


#pragma mark - Private Methods -
/*
 *  Private helpers.
 */

- (void)preloadAdMobAd
{
    m_interstitial = [[GADInterstitial alloc] initWithAdUnitID:m_adMobUnitID];
    m_interstitial.delegate = self;
    
    GADRequest *request = [GADRequest request];
    
    request.testDevices = m_adMobTestDevices;
    
    [m_interstitial loadRequest:request];
    
}


#pragma mark - Delegate Methods -
/*
 *  Methods from the GADInterstitialDelegate protocol
 */

- (void)interstitialDidDismissScreen:(GADInterstitial *)interstitial
{
    NSLog(@"AdMob ad was dismissed.");
    
    // Preload a brand new ad while dismissing the old ad.
    [self preloadAdMobAd];
    
    [[AdManager sharedInstance] adWasDismissed];
}

@end
