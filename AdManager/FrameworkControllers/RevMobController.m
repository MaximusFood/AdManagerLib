//
//  FlurryController.m
//  AdManager
//
//  Created by Maxim Arnold on 22/11/2015.
//  Copyright © 2015 Maxim. All rights reserved.
//

#import "RevMobController.h"
#import "AdFrameworkController+Internal.h"
#import "AdManager+Internal.h"

@implementation RevMobController
{
    NSString* m_mediaID;
    
    RevMobFullscreen* m_fullscreenAd;
}

@synthesize mediaID = m_mediaID;


/*
 *  Initialisation
 */

// Factory initialiser.
+ (id)controller
{
    return [[self alloc] initWithFrameworkType:kFrameworkTypeRevMob];
}


#pragma mark - Internal Methods -
/*
 *  Overridden Internal Methods
 */

- (void)prepareFramework
{
    if(![m_mediaID isEqualToString:@""])
    {
        [RevMobAds startSessionWithAppID:m_mediaID andDelegate:self];
    }
}

- (BOOL)attemptShowAd
{
    BOOL success = FALSE;
    
    NSLog(@"Attempting to show a RevMob ad...");
    
    if(m_fullscreenAd)
    {
        [m_fullscreenAd showAd];
        success = TRUE;
        NSLog(@"Should be seeing a RevMob ad");
        
        m_firstAdShown = NO;
        if( ![AdManager sharedInstance].adHasBeenShown )
            m_firstAdShown = YES;
    }
    
    return success;
}


#pragma mark - Delegate Methods -
/*
 *  Methods from the RevMobAdsDelegate protocol
 */

- (void)revmobSessionIsStarted
{
    NSLog(@"[RevMob Sample App] Session started with delegate.");
    // Have to wait until we're connected before we set up the ad
    m_fullscreenAd = [[RevMobAds session] fullscreen];
    m_fullscreenAd.delegate = self;
    [m_fullscreenAd loadAd];
}


- (void)revmobAdDidFailWithError:(NSError *)error
{
    NSLog(@"Revmob Ad failed with error: %@", error);
    [[AdManager sharedInstance] failedToShowAd:m_firstAdShown];
}

- (void)revmobAdDidReceive
{
    NSLog(@"Revmob Ad loaded successfullly");
}

- (void)revmobAdDisplayed
{
    NSLog(@"Revmob Ad displayed");
}

- (void)revmobUserClosedTheAd
{
    NSLog(@"User closed the Revmob ad");
    [[AdManager sharedInstance] adWasDismissed];
}


@end
