//
//  FlurryController.h
//  AdManager
//
//  Created by Maxim Arnold on 22/11/2015.
//  Copyright © 2015 Maxim. All rights reserved.
//

#import "AdFrameworkController.h"
#import <VungleSDK/VungleSDK.h>


@interface VungleController : AdFrameworkController <VungleSDKDelegate>

@property (nonatomic, strong) NSString* appID;


+ (id)controller;

@end
