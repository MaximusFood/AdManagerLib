//
//  IAdController.m
//  AdManager
//
//  Created by Maxim Arnold on 21/11/2015.
//  Copyright © 2015 Maxim. All rights reserved.
//

#import "IAdController.h"
#import "AdManager+Internal.h"
#import "AdFrameworkController+Internal.h"

@implementation IAdController
{
    ADInterstitialAd* m_iAdInterstitial;
}


/*
 *  Initialisation
 */

// Factory initialiser.
+ (id)controller
{
    return [[self alloc] initWithFrameworkType:kFrameworkTypeIAd];
}

- (id)initWithFrameworkType:(NSString *)frameworkType
{
    self = [super initWithFrameworkType:frameworkType];
    if(!self) return nil;
    
    m_iAdInterstitial = nil;
    
    return self;
}


#pragma mark - Internal Methods -
/*
 *  Overridden Internal Methods
 */

- (BOOL)attemptShowAd
{
    BOOL success = FALSE;
    
    NSLog(@"Attempting to show an iAd...");
    
    m_iAdInterstitial = [[ADInterstitialAd alloc] init];
    m_iAdInterstitial.delegate = self;
    [AdManager sharedInstance].uiViewController.interstitialPresentationPolicy = ADInterstitialPresentationPolicyManual;
    if([[AdManager sharedInstance].uiViewController requestInterstitialAdPresentation])
    {
        success = TRUE;
        NSLog(@"Should be seeing an iAd");
    }
    
    return success;
}


#pragma mark - Delegate Methods -
/*
 *  Methods from the ADInterstitialAdDelegate protocol
 */

-(void)interstitialAd:(ADInterstitialAd *)interstitialAd didFailWithError:(NSError *)error
{
    m_iAdInterstitial = nil;
    //m_iAdInterstitial.delegate = nil;
    NSLog(@"iAd interstitialAd didFailWithERROR");
    NSLog(@"%@", error);
}

-(void)interstitialAdDidLoad:(ADInterstitialAd *)interstitialAd
{
    NSLog(@"iAd interstitialAdDidLOAD");
    if (interstitialAd != nil && m_iAdInterstitial != nil )
    {
        [m_iAdInterstitial presentInView:[AdManager sharedInstance].uiViewController.view];
        NSLog(@"iAd interstitialAdDidPRESENT");
    }
}

-(void)interstitialAdDidUnload:(ADInterstitialAd *)interstitialAd
{
    m_iAdInterstitial = nil;
    //[interstitialAd release];
    NSLog(@"iAd interstitialAdDidUNLOAD");
}

-(void)interstitialAdActionDidFinish:(ADInterstitialAd *)interstitialAd
{
    m_iAdInterstitial = nil;
    //m_iAdInterstitial.delegate = nil;
    NSLog(@"iAd interstitialAdDidFINISH");
    
    // iAd is calling this method willy nilly, so I'm gonna have to take this out for the
    // time being until I work out what to do. 
    //[[AdManager sharedInstance] adWasDismissed];
}

@end
