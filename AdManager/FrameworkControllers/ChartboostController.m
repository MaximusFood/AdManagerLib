//
//  ChartboostController.m
//  AdManager
//
//  Created by Maxim Arnold on 21/11/2015.
//  Copyright © 2015 Maxim. All rights reserved.
//

#import "ChartboostController.h"
#import "AdManager+Internal.h"
#import "AdFrameworkController+Internal.h"

@implementation ChartboostController
{
    NSString* m_cbAppID;
    NSString* m_cbAppSig;
}


@synthesize cbAppID = m_cbAppID;
@synthesize cbAppSig = m_cbAppSig;


/*
 *  Initialisation
 */

// Factory initialiser.
+ (id)controller
{
    return [[self alloc] initWithFrameworkType:kFrameworkTypeChartboost];
}

- (id)initWithFrameworkType:(NSString *)frameworkType
{
    self = [super initWithFrameworkType:frameworkType];
    if(!self) return nil;
    
    m_cbAppID = @"";
    m_cbAppSig = @"";
    
    return self;
}


#pragma mark - Internal Methods -
/*
 *  Overridden Internal Methods
 */

- (void)prepareFramework
{
    [self initCharboost];
}

- (BOOL)attemptShowAd
{
    BOOL success = FALSE;
    
    NSLog(@"Attempting to show a Chartboost ad...");
    
    if( [Chartboost hasInterstitial:CBLocationHomeScreen] )
    {
        [Chartboost showInterstitial:CBLocationHomeScreen];
        // We don't actually know here if the chartboost attempt was successful.
        // didFailToLoadInterstitial is called if it fails, so in this method we try
        // again, resetting the queue and stuff.
        success = TRUE;
        
        // Should never be YES at this point. Little bit easier than resetting it before
        // attempting.
        m_firstAdShown = NO;
        
        // In this case we are going to be setting m_adHasBeenShown to true, even
        // though we may eventually not show a cb ad. Therefore we need a flag so we
        // can know whether or not to flip the flag should this ad fail. Only if the
        // flag had not already been set, of course.
        if( ![AdManager sharedInstance].adHasBeenShown )
            m_firstAdShown = YES;
        
        NSLog(@"Should be seeing a Chartboost ad");
    }
    else
    {
        [Chartboost cacheInterstitial:CBLocationHomeScreen];
    }
    
    return success;
}


#pragma mark - Private Methods -
/*
 *  Private helpers.
 */

- (void)initCharboost
{
    // Initialize the Chartboost library
    [Chartboost startWithAppId:m_cbAppID
                  appSignature:m_cbAppSig
                      delegate:self];
    
    if( ![Chartboost hasInterstitial:CBLocationHomeScreen] )
        [Chartboost cacheInterstitial:CBLocationHomeScreen];
}


#pragma mark - Delegate Methods -
/*
 *  Methods from the ChartboostDelegate protocol
 */

- (BOOL)shouldRequestInterstitialsInFirstSession
{
    return NO;
}

- (void)didDismissInterstitial:(NSString *)location
{
    [Chartboost cacheInterstitial:CBLocationHomeScreen];
    
    NSLog(@"dismissed Chartboost interstitial at location %@", location);
    
    [[AdManager sharedInstance] adWasDismissed];
}

- (void)didFailToLoadInterstitial:(NSString *)location withError:(CBLoadError)error {
    
    switch(error){
        case CBLoadErrorInternetUnavailable: {
            NSLog(@"Failed to load Chartboost Interstitial, no Internet connection !");
        } break;
        case CBLoadErrorInternal: {
            NSLog(@"Failed to load Chartboost Interstitial, internal error !");
        } break;
        case CBLoadErrorNetworkFailure: {
            NSLog(@"Failed to load Chartboost Interstitial, network error !");
        } break;
        case CBLoadErrorWrongOrientation: {
            NSLog(@"Failed to load Chartboost Interstitial, wrong orientation !");
        } break;
        case CBLoadErrorTooManyConnections: {
            NSLog(@"Failed to load Chartboost Interstitial, too many connections !");
        } break;
        case CBLoadErrorFirstSessionInterstitialsDisabled: {
            NSLog(@"Failed to load Chartboost Interstitial, first session !");
        } break;
        case CBLoadErrorNoAdFound : {
            NSLog(@"Failed to load Chartboost Interstitial, no ad found !");
        } break;
        case CBLoadErrorSessionNotStarted : {
            NSLog(@"Failed to load Chartboost Interstitial, session not started !");
        } break;
        case CBLoadErrorNoLocationFound : {
            NSLog(@"Failed to load Chartboost Interstitial, missing location parameter !");
        } break;
        default: {
            NSLog(@"Failed to load Chartboost Interstitial, unknown error !");
        }
    }
    
    [[AdManager sharedInstance] failedToShowAd:m_firstAdShown];
}

@end
