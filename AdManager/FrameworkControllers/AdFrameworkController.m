//
//  AdFrameworkController.m
//  AdManager
//
//  Created by Maxim Arnold on 21/11/2015.
//  Copyright © 2015 Maxim. All rights reserved.
//

#import "AdFrameworkController.h"
#import "AdFrameworkController+Internal.h"

@implementation AdFrameworkController

@synthesize frameworkType = m_frameworkType;

@end


/*
 *  Internal implementation
 */

@implementation AdFrameworkController (Internal)

/*
 *  Initialisation
 */

- (id)initWithFrameworkType:(NSString *)frameworkType
{
    self = [super init];
    if(!self) return nil;
    
    m_frameworkType = frameworkType;
    
    m_firstAdShown = NO;
    
    return self;
}


/*
 *  Internal Methods
 */

- (void)prepareFramework
{
    // Not all frameworks need to do anything here. 
}


- (BOOL)attemptShowAd
{
    return FALSE;
}

@end
