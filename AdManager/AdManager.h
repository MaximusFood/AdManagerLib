//
//  AdManager.h
//  v1.4.2
//
//  Created by Maxim Arnold on 14/10/2015.
//  Copyright © 2015 Maxim. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;


@protocol AdManagerObserver

@optional

/*
 *  Called, if possible, when an ad is dismissed, from any framework.
 */
- (void)adDismissed;

@end


# pragma mark - 

/*
 *  AdFrameworkController forward declarations
 */
@class ChartboostController;
@class IAdController;
@class AdMobController;
@class FlurryController;
@class VungleController;
@class RevMobController;


/*
 *  Framework Identifiers.
 *  Use if providing a priority list.
 *  Why not an enum? We're using these as dictionary keys, and it's a proper pain to be
 *  boxing and unboxing all the time.
 */
static NSString* kFrameworkTypeChartboost = @"C";
static NSString* kFrameworkTypeAdMob = @"A";
static NSString* kFrameworkTypeIAd = @"I";
static NSString* kFrameworkTypeFlurry = @"F";
static NSString* kFrameworkTypeVungle = @"V";
static NSString* kFrameworkTypeRevMob = @"R";


@interface AdManager : NSObject

/*
 *  AdManager is a singleton. Call sharedInstance to access it.
 */
+ (AdManager *)sharedInstance;


# pragma mark - Pubilc Properties -

/*
 *  Shared Properties
 */

// Essential. Won't run without it.
@property(nonatomic, strong) UIViewController* uiViewController;

// Kill Switch. Use for things like Remove Ads IAPs and stuff. Easier than conditionally
// calling attemptAdIfReady all over your code.
@property(nonatomic) BOOL suppressAllAds;

// Set the priority of the different frameworks.
// Use the framework identifier constants listed above as keys, with ints as the values.
// Frameworks with the lowest numbers are attempted first. If multiple frameworks have
// equal values, then we randomly choose between them.
// Default is all equal, which means the first framework to try is chosen at random.
@property(nonatomic, strong) NSDictionary* priorityList;

// Number of attemptAdIfReady calls to ignore before trying to show another ad.
// Defaults to 5.
@property(nonatomic) int cooldownAttempts;

// Should be between 0 - 1. Chance of showing an ad outside of a cooldown period.
// Defaults to 0.5.
@property(nonatomic) double chanceOfAttempt;

// True if we have successfully shown an ad since the AdManager was initialised.
@property(nonatomic, readonly) BOOL adHasBeenShown;


/*
 *  Ad Framework Controllers
 *  There'll be a separate controller for each supported framework, and their interfaces
 *  are be exported, so we can access them directly to do framework specific stuff.
 */
@property(nonatomic, strong, readonly) ChartboostController* chartboostController;
@property(nonatomic, strong, readonly) IAdController* iAdController;
@property(nonatomic, strong, readonly) AdMobController* adMobController;
@property(nonatomic, strong, readonly) FlurryController* flurryController;
@property(nonatomic, strong, readonly) VungleController* vungleController;
@property(nonatomic, strong, readonly) RevMobController* revMobController;


# pragma mark - Pubilc Methods -

// Loads up the AdMob and Chartboost frameworks. Needs to be called early on, probs when
// you're setting up the AdManager.
- (void)prepareAdFrameworks;

// The main method. Call this dude when you want to show an ad. Whether or not we show an
// ad on a call to this method depends on:
// 1)   The cooldownAttempts property. After successfully showing an ad we ignore this many
//      calls to attemptAdIfReady before trying again.
// 2)   The chanceOfAttempt property. Outside of a cooldown period on a call to attemptAdIfReady
//      there is a 0 - 1 chance of us attempting an ad.
// 3)   Ad availability. If after trying all frameworks in the priority list and failing,
//      we will try again on the next call to attemptAdIfReady.
// The order of the framework attempts can be provided with the priorityList property.
- (void)attemptAdIfReady;


// There are a few callbacks in the AdManagerObserver above. To listen out for them,
// simply implement them somewhere, and register the class with this method.
- (void)addAdManagerObserver:(NSObject<AdManagerObserver>*)observer;

// And remove...
- (void)removeAdManagerObserver:(NSObject<AdManagerObserver>*)observer;

@end

