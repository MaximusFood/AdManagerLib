//
//  AdManager+Internal.h
//  AdManager
//
//  Created by Maxim Arnold on 21/11/2015.
//  Copyright © 2015 Maxim. All rights reserved.
//
//  Sometimes the framework controllers will need to call back to the AdManager. This
//  will be using methods that shouldn't be available outside the library, so we use an
//  'internal' catagory, that is not exported.
//

#import "AdManager.h"

@interface AdManager (Internal)

// Called when a 'failed to show ad' kind of framework delegate is called. Some frameworks
// work this way -- not telling us from the off that our attempt is doomed.
- (void)failedToShowAd:(BOOL)firstAd;

// Called when the ad is dismissed. Most, if not all, framework delegates have one of
// these methods. We want to know so we can tell an AdManager observer. 
- (void)adWasDismissed;

@end
